# Simple Cyberpunk System #

This system is inspired by Risus, Shadowrun, and D6. 
MIT License.

## Rolling Mechanic ##

SCS is a D6 dicepool system. When you roll, roll an amount of D6s equal to the stat. 5 or 6 is a success, and 1 negates a success. Some actions might require additional successes. You cannot roll any stat with a 0 in it.

### Competitions ###

If you are trying to roll against another character, you both roll the relevant stat for your action. Whoever has the most successes wins. When you fail a roll in a competition, you temporarily lose a point in that stat until you have time to rest.

## Stats ##

You have 10 points to distribute across the following 5 stats. You cannot have more than 5 points in a score.

**Body** Your ability to do athletics, get a direct shot in a stressful situation, and any weird back-alley surgeries you've gotten.  
**Computer** Your ability to hack, your software, and where to find it.  
**Tech** Your cybernetic enhancements, cars, and toys.  
**Face** Your ability to make a first impression.  
**Contacts** Your history of knowing useful people.  

### Inventory ###

Your starting gear is dependant upon your stats. If you have a 1 in Computer, you probably have a nice cellphone. If you have a 5 in Computer, you probably have a full hacking suite capable of breaking into almost anything.

### Unique Abilities (Optional) ###

The GM Might grant an additional 2 points. When they do that you may use ONE point to gain a unique ability. This might be an experimental car, a cybernetic enhancement that can do something far beyond the expectations of known cybernetics, or an infinite supply of some sort of super drug.